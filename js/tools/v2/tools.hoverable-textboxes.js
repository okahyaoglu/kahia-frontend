﻿function initHoverableTextboxes() {
    jQuery('[default-text]').each(function(index, elem) {
        var $elem = jQuery(elem);
        var defaultText = $elem.attr('default-text');
        $elem.val(defaultText);

        $elem.off('focus');
        $elem.on('focus', function() {
            if (jQuery(this).val() == defaultText)
                jQuery(this).val('');
        });

        $elem.off('blur');
        $elem.on('blur', function() {
            if (jQuery(this).val() == '')
                jQuery(this).val(defaultText);
        });
    });
}
jQuery(function() {
    initHoverableTextboxes();
    tools.updatePanel.onRequestEnd(initHoverableTextboxes);
});