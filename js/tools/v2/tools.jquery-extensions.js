﻿///prev() metodunu extend eder. Önceki eleman yoksa, en sondakini seçer. slider gibi.
//belirli bir kriter içinde gezinmek istiyorsanız, selector'u her seferinde vermeniz gerekiyor.
jQuery.fn.prevOrLast = function (selector) {
    if (selector) {
        var children = this.parent().children(selector);
        var index = children.index(this);
        if (index == 0)
            return children.last();
        return children.eq(index - 1);
    } else {
        if (this.is(':first-child'))
            return this.parent().children(':last-child');
        return this.prev();
    }
}

///next() metodunu extend eder. sonraki eleman yoksa, ilkini seçer. slider gibi.
jQuery.fn.nextOrFirst = function (selector) {
    if (selector) {
        var children = this.parent().children(selector);
        var index = children.index(this);
        if (index == children.length - 1)
            return children.first();
        return children.eq(index + 1);
    } else {
        if (this.is(':last-child'))
            return this.parent().children(':first-child');
        return this.next(selector);
    }
}

// length check etmekten kurtarıyor. if($('link').any())
jQuery.fn.any = function (selector) {
    return selector ? this.filter(selector).length != 0 : this.length != 0;
}

//Case-insensitive ":contains" selector
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function (arg) {
    return function (elem) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//load scriptin cache'lenebilir versiyonu. $.done (promise şekilnde kullanılabiliyor.
jQuery.cachedScript = function (url, options) {
    options = $.extend(options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });
    return jQuery.ajax(options);
};

//The handler is executed at most once per element for all event types.
//$(document).one('... ... ... ..') gibi birden çok event verildiğinde, her event için bir kere çağırıyor .one.
//bu metod, totalde bir kere çağırılmasını sağlıyor.
$.fn.once = function (events, callback) {
    return this.each(function () {
        $(this).on(events, myCallback);
        function myCallback(e) {
            $(this).off(events, myCallback);
            callback.call(this, e);
        }
    });
};