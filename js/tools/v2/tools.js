﻿var tools = tools || {};

var log = isLocalMode & typeof (console) != 'undefined' & typeof (console.log) == 'function' ? console.log.bind(console) : function () { };
//-----------------------------
//    Image Preloading
//-----------------------------
tools.preloadImages = function(picture_urls, callback) {
    var loaded = 0;
    for (var i = 0, j = picture_urls.length; i < j; i++) {
        var img = new Image();
        var url = picture_urls[i];
        img.onload = function () {
            if (++loaded == picture_urls.length && callback) {
                callback(url);
            }
        }
        img.src = url;
    }
};

//-----------------------------
//    Scrolling
//-----------------------------
tools.scroll = {};
tools.scroll.to = function (pos, duration, callback) {
    $('html,body').animate({ scrollTop: pos }, duration, callback);
};
tools.scroll.formOnLoad = function(titleSelector, messageSelector, delay, offsetTop) {
    offsetTop = offsetTop || 0;
    $(document).one('ready pageshow', function() {
        setTimeout(function() {
            var container = $.mobile ? $.mobile.activePage : document;
            var top = $(titleSelector, container).offset().top;
            tools.scrollTo(top - offsetTop, 'slow', function() {
                $(messageSelector, container).effect('highlight');
            });
        }, delay);
    });
}