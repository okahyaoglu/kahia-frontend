﻿//-----------------------------
//    UpdatePanel Scripts
//-----------------------------
tools.updatePanel = {};
tools.updatePanel._init = function () {
    var prm = null;
    try {
        prm = Sys.WebForms.PageRequestManager.getInstance();
    } catch (e) {
        log('Exception occured while accessing pageRequestManager. It might not be included in page?');
        return null;
    }
    return prm;
};

tools.updatePanel._logErrors = function (args) {
    if (args == null)
        return;

    var error = args.get_error();
    if (error != null) {
        args.set_errorHandled(true);
        var msg = error.message.replace("Sys.WebForms.PageRequestManagerServerErrorException: ", "");
        log(msg);
    }
};

tools.updatePanel.onRequestEnd = function (functionToBeInvoked) {
    var prm = tools.updatePanel._init();
    if (prm == null)
        return;

    // Called when async postback ends
    prm.add_endRequest(function (sender, args) {
        functionToBeInvoked(sender._postBackSettings.sourceElement);
        try { tools.updatePanel._logErrors(args); } catch (e) { }
    });
};

tools.updatePanel.onRequestStart = function (functionToBeInvoked) {
    var prm = tools.updatePanel._init();
    if (prm == null)
        return;

    // Called when async postback begins
    prm.add_initializeRequest(function (sender, args) {
        functionToBeInvoked(sender._postBackSettings.sourceElement);
        try { tools.updatePanel._logErrors(args); } catch (e) { }
    });
};