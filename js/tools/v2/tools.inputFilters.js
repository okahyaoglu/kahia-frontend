﻿//-----------------------------
//    Textbox Input Filtering
//-----------------------------
tools.inputFilters = {};
//tools.inputFilters.allowOnlyNumbers(selector, {allowSpace:true, allowPlus : true, allowMinus : true});
tools.inputFilters.allowOnlyNumbers = function(selector, options) {
    var options = options || {};
    var allowSpace = options.allowSpace || false;
    var allowPlus = options.allowPlus || false;
    var allowMinus = options.allowMinus || false;
    $(document).on('keypress', selector, function(e) {
        if (e.which == 8 //backspace
        || e.which == 46 //delete
        || e.which == 0 //?
        || e.which == 35 //HOME
        || e.which == 36 //END
        || (e.which == 32 && allowSpace) //space
        || (e.which == 43 && allowPlus) //plus
        || (e.which == 45 && allowMinus) //plus
        || (e.which >= 48 && e.which <= 57)) //numbers
            return true;
        return false;
    });
}
tools.inputFilters.disableNumbers = function(selector) {
    $(document).on('keyup blur', selector, function() {
        $(this).val($(this).val().replace(/[^a-zA-ZçÇıİöÖüÜşŞğĞ .]/g, ''));
    });
}