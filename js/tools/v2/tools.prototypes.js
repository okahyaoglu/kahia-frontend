﻿//-----------------------------
//    Prototypes
//-----------------------------
String.prototype.format = function () {
    /// <summary>
    /// {0} şeklinde c#daki gibi stringi değiştirmenizi sağlar.
    /// </summary>
    function parseDateStr(rgx, value) {
        //(Oğuzhan) eğer burada hata alınıyorsa, regexiniz hatalıdır.
        var dateFormat = rgx.split(':')[1].replace('}', '');
        return $.datepicker.formatDate(dateFormat, value);
    }
    var a = arguments;
    return this.replace(/{(\d+)(:[dmy\.\,\-]*)?}/g, function (m, i) {
        if (a[i]) {
            //for dates like {0:dd}
            return m.indexOf(':') != -1 ? parseDateStr(m, a[i]) : a[i];
        }
        return "";
    });
};
String.prototype.formatUsingObject = function (obj) {
    /// <summary>
    /// String.format'taki gibi {0} şeklinde değil de, verilen objenin property'lerini kullanarak formatlama yapar. 
    /// Ör: "{title} {id}" gibi bir stringi {title:"65", id:5} gibi bir objeyle formatlamanızı sağlar.
    /// </summary>
    var result = template;
    for (var key in obj) {
        result = result.replace("{" + key + "}", obj[key]);
    }
    return result;
};

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

String.prototype.replaceCaseInsensitive = function (strReplace) {
    /// <summary>
    /// Case-Insensitive olarak replace yapar. Fakat JS kültürü ingilizce olduğundan, türkçe karakterlerde düzgün çalışmayacaktır.
    /// </summary>
    var pattern = new RegExp(strReplace, "gi");
    var replacement = "<strong>$&</strong>";
    return this.replace(pattern, replacement);
};

String.prototype.turkishToUpper = function () {
    var string = this;
    var letters = { "i": "İ", "ş": "Ş", "ğ": "Ğ", "ü": "Ü", "ö": "Ö", "ç": "Ç", "ı": "I" };
    var result = string.replace(/(([iışğüçö]))+/g, function (letter) { return letters[letter] || letter; });
    return result.toUpperCase();
}

String.prototype.turkishToLower = function () {
    var string = this;
    var letters = { "İ": "i", "I": "ı", "Ş": "ş", "Ğ": "ğ", "Ü": "ü", "Ö": "ö", "Ç": "ç" };
    var result = string.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) { return letters[letter] || letter; });
    return result.toLowerCase();
}

String.prototype.toEnglishStr = function () {
    var str = this;
    var letters = { "İ": "I", "Ş": "S", "Ğ": "G", "Ü": "U", "Ö": "O", "Ç": "C", "ı": "i", "ş": "s", "ğ": "g", "ü": "u", "ö": "o", "ç": "c" };
    var result = str.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) { return letters[letter] || letter; });
    return result;
}


Array.prototype.contains = function (v) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};

Array.prototype.unique = function () {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
}

