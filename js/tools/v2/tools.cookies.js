﻿//-----------------------------
//    Cookies: Get/Set
//-----------------------------
tools.cookies = {};
tools.cookies.get = function(cookieName) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + cookieName + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(cookieName + "=");
    }
    if (c_start == -1) {
        c_value = null;
    } else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
};

tools.cookies.set = function(cookieName, value, daysToStay) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + daysToStay);
    var c_value = escape(value) + ((daysToStay == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = cookieName + "=" + c_value;
};

tools.cookies.check = function(cookieName) {
    var cookie = tools.cookies.get(cookieName);
    return cookie != null && cookie != "";
};