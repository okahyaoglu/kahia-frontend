﻿/*
{ 
    serviceURL (scriptServiceUrl) 
    controlLocation (userControlPath || uc) 
    selector, 
    successCallback, (onSuccess || success)
    errorCallback, (errorCallback || error)
    extraQueryString (params)
    append (shouldAppend)
}
*/
var ucBinder = function (options) {
    var serviceUrl = options.serviceURL || options.scriptServiceUrl || scriptServiceUrl;
    if (!serviceUrl)
        throw "serviceURL olmadan ucBinder kullanılamaz";
    var userControlPath = options.controlLocation || options.userControlPath || options.uc;
    var extraQueryString = options.extraQueryString || options.params || "";
    var onSuccess = options.successCallback || options.onSuccess || options.success || function () { };
    var onError = options.errorCallback || options.onError || options.error || function () { };
    var shouldAppend = options.shouldAppend || options.append;

    var url = serviceUrl + "?isMobileRedirectionDisabled=1&controlLocation=" + userControlPath + "&lang=" + language + extraQueryString;
    $.ajax({
        type: "GET",
        url: url,
        timeout: options.timeout,
        dataType: 'text',
        success: function (html) {
            var $elem = $(options.selector);
            if (html == "" || html.indexOf("!!error!!") != -1) {
                $elem.remove();
                onError();
                return;
            }
            if (shouldAppend)
                $elem.show().append(html);
            else
                $elem.show().html(html);
            if (onSuccess)
                onSuccess(html, $elem);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $(options.selector).remove();
            onError(XMLHttpRequest, textStatus, errorThrown);
        }
    });
};