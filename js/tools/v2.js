﻿///#source 1 1 /js/tools/v2/tools.js
var tools = tools || {};

var log = isLocalMode & typeof (console) != 'undefined' & typeof (console.log) == 'function' ? console.log.bind(console) : function () { };
//-----------------------------
//    Image Preloading
//-----------------------------
tools.preloadImages = function(picture_urls, callback) {
    var loaded = 0;
    for (var i = 0, j = picture_urls.length; i < j; i++) {
        var img = new Image();
        var url = picture_urls[i];
        img.onload = function () {
            if (++loaded == picture_urls.length && callback) {
                callback(url);
            }
        }
        img.src = url;
    }
};

//-----------------------------
//    Scrolling
//-----------------------------
tools.scroll = {};
tools.scroll.to = function (pos, duration, callback) {
    $('html,body').animate({ scrollTop: pos }, duration, callback);
};
tools.scroll.formOnLoad = function(titleSelector, messageSelector, delay, offsetTop) {
    offsetTop = offsetTop || 0;
    $(document).one('ready pageshow', function() {
        setTimeout(function() {
            var container = $.mobile ? $.mobile.activePage : document;
            var top = $(titleSelector, container).offset().top;
            tools.scrollTo(top - offsetTop, 'slow', function() {
                $(messageSelector, container).effect('highlight');
            });
        }, delay);
    });
}
///#source 1 1 /js/tools/v2/tools.cookies.js
//-----------------------------
//    Cookies: Get/Set
//-----------------------------
tools.cookies = {};
tools.cookies.get = function(cookieName) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + cookieName + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(cookieName + "=");
    }
    if (c_start == -1) {
        c_value = null;
    } else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
};

tools.cookies.set = function(cookieName, value, daysToStay) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + daysToStay);
    var c_value = escape(value) + ((daysToStay == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = cookieName + "=" + c_value;
};

tools.cookies.check = function(cookieName) {
    var cookie = tools.cookies.get(cookieName);
    return cookie != null && cookie != "";
};
///#source 1 1 /js/tools/v2/tools.hoverable-textboxes.js
function initHoverableTextboxes() {
    jQuery('[default-text]').each(function(index, elem) {
        var $elem = jQuery(elem);
        var defaultText = $elem.attr('default-text');
        $elem.val(defaultText);

        $elem.off('focus');
        $elem.on('focus', function() {
            if (jQuery(this).val() == defaultText)
                jQuery(this).val('');
        });

        $elem.off('blur');
        $elem.on('blur', function() {
            if (jQuery(this).val() == '')
                jQuery(this).val(defaultText);
        });
    });
}
jQuery(function() {
    initHoverableTextboxes();
    tools.updatePanel.onRequestEnd(initHoverableTextboxes);
});
///#source 1 1 /js/tools/v2/tools.inputFilters.js
//-----------------------------
//    Textbox Input Filtering
//-----------------------------
tools.inputFilters = {};
//tools.inputFilters.allowOnlyNumbers(selector, {allowSpace:true, allowPlus : true, allowMinus : true});
tools.inputFilters.allowOnlyNumbers = function(selector, options) {
    var options = options || {};
    var allowSpace = options.allowSpace || false;
    var allowPlus = options.allowPlus || false;
    var allowMinus = options.allowMinus || false;
    $(document).on('keypress', selector, function(e) {
        if (e.which == 8 //backspace
        || e.which == 46 //delete
        || e.which == 0 //?
        || e.which == 35 //HOME
        || e.which == 36 //END
        || (e.which == 32 && allowSpace) //space
        || (e.which == 43 && allowPlus) //plus
        || (e.which == 45 && allowMinus) //plus
        || (e.which >= 48 && e.which <= 57)) //numbers
            return true;
        return false;
    });
}
tools.inputFilters.disableNumbers = function(selector) {
    $(document).on('keyup blur', selector, function() {
        $(this).val($(this).val().replace(/[^a-zA-ZçÇıİöÖüÜşŞğĞ .]/g, ''));
    });
}
///#source 1 1 /js/tools/v2/tools.jquery-extensions.js
///prev() metodunu extend eder. Önceki eleman yoksa, en sondakini seçer. slider gibi.
//belirli bir kriter içinde gezinmek istiyorsanız, selector'u her seferinde vermeniz gerekiyor.
jQuery.fn.prevOrLast = function (selector) {
    if (selector) {
        var children = this.parent().children(selector);
        var index = children.index(this);
        if (index == 0)
            return children.last();
        return children.eq(index - 1);
    } else {
        if (this.is(':first-child'))
            return this.parent().children(':last-child');
        return this.prev();
    }
}

///next() metodunu extend eder. sonraki eleman yoksa, ilkini seçer. slider gibi.
jQuery.fn.nextOrFirst = function (selector) {
    if (selector) {
        var children = this.parent().children(selector);
        var index = children.index(this);
        if (index == children.length - 1)
            return children.first();
        return children.eq(index + 1);
    } else {
        if (this.is(':last-child'))
            return this.parent().children(':first-child');
        return this.next(selector);
    }
}

// length check etmekten kurtarıyor. if($('link').any())
jQuery.fn.any = function (selector) {
    return selector ? this.filter(selector).length != 0 : this.length != 0;
}

//Case-insensitive ":contains" selector
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function (arg) {
    return function (elem) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//load scriptin cache'lenebilir versiyonu. $.done (promise şekilnde kullanılabiliyor.
jQuery.cachedScript = function (url, options) {
    options = $.extend(options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });
    return jQuery.ajax(options);
};

//The handler is executed at most once per element for all event types.
//$(document).one('... ... ... ..') gibi birden çok event verildiğinde, her event için bir kere çağırıyor .one.
//bu metod, totalde bir kere çağırılmasını sağlıyor.
$.fn.once = function (events, callback) {
    return this.each(function () {
        $(this).on(events, myCallback);
        function myCallback(e) {
            $(this).off(events, myCallback);
            callback.call(this, e);
        }
    });
};
///#source 1 1 /js/tools/v2/tools.prototypes.js
//-----------------------------
//    Prototypes
//-----------------------------
String.prototype.format = function () {
    /// <summary>
    /// {0} şeklinde c#daki gibi stringi değiştirmenizi sağlar.
    /// </summary>
    function parseDateStr(rgx, value) {
        //(Oğuzhan) eğer burada hata alınıyorsa, regexiniz hatalıdır.
        var dateFormat = rgx.split(':')[1].replace('}', '');
        return $.datepicker.formatDate(dateFormat, value);
    }
    var a = arguments;
    return this.replace(/{(\d+)(:[dmy\.\,\-]*)?}/g, function (m, i) {
        if (a[i]) {
            //for dates like {0:dd}
            return m.indexOf(':') != -1 ? parseDateStr(m, a[i]) : a[i];
        }
        return "";
    });
};
String.prototype.formatUsingObject = function (obj) {
    /// <summary>
    /// String.format'taki gibi {0} şeklinde değil de, verilen objenin property'lerini kullanarak formatlama yapar. 
    /// Ör: "{title} {id}" gibi bir stringi {title:"65", id:5} gibi bir objeyle formatlamanızı sağlar.
    /// </summary>
    var result = template;
    for (var key in obj) {
        result = result.replace("{" + key + "}", obj[key]);
    }
    return result;
};

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

String.prototype.replaceCaseInsensitive = function (strReplace) {
    /// <summary>
    /// Case-Insensitive olarak replace yapar. Fakat JS kültürü ingilizce olduğundan, türkçe karakterlerde düzgün çalışmayacaktır.
    /// </summary>
    var pattern = new RegExp(strReplace, "gi");
    var replacement = "<strong>$&</strong>";
    return this.replace(pattern, replacement);
};

String.prototype.turkishToUpper = function () {
    var string = this;
    var letters = { "i": "İ", "ş": "Ş", "ğ": "Ğ", "ü": "Ü", "ö": "Ö", "ç": "Ç", "ı": "I" };
    var result = string.replace(/(([iışğüçö]))+/g, function (letter) { return letters[letter] || letter; });
    return result.toUpperCase();
}

String.prototype.turkishToLower = function () {
    var string = this;
    var letters = { "İ": "i", "I": "ı", "Ş": "ş", "Ğ": "ğ", "Ü": "ü", "Ö": "ö", "Ç": "ç" };
    var result = string.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) { return letters[letter] || letter; });
    return result.toLowerCase();
}

String.prototype.toEnglishStr = function () {
    var str = this;
    var letters = { "İ": "I", "Ş": "S", "Ğ": "G", "Ü": "U", "Ö": "O", "Ç": "C", "ı": "i", "ş": "s", "ğ": "g", "ü": "u", "ö": "o", "ç": "c" };
    var result = str.replace(/(([İIŞĞÜÇÖ]))+/g, function (letter) { return letters[letter] || letter; });
    return result;
}


Array.prototype.contains = function (v) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};

Array.prototype.unique = function () {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
}


///#source 1 1 /js/tools/v2/tools.updatepanel.js
//-----------------------------
//    UpdatePanel Scripts
//-----------------------------
tools.updatePanel = {};
tools.updatePanel._init = function () {
    var prm = null;
    try {
        prm = Sys.WebForms.PageRequestManager.getInstance();
    } catch (e) {
        log('Exception occured while accessing pageRequestManager. It might not be included in page?');
        return null;
    }
    return prm;
};

tools.updatePanel._logErrors = function (args) {
    if (args == null)
        return;

    var error = args.get_error();
    if (error != null) {
        args.set_errorHandled(true);
        var msg = error.message.replace("Sys.WebForms.PageRequestManagerServerErrorException: ", "");
        log(msg);
    }
};

tools.updatePanel.onRequestEnd = function (functionToBeInvoked) {
    var prm = tools.updatePanel._init();
    if (prm == null)
        return;

    // Called when async postback ends
    prm.add_endRequest(function (sender, args) {
        functionToBeInvoked(sender._postBackSettings.sourceElement);
        try { tools.updatePanel._logErrors(args); } catch (e) { }
    });
};

tools.updatePanel.onRequestStart = function (functionToBeInvoked) {
    var prm = tools.updatePanel._init();
    if (prm == null)
        return;

    // Called when async postback begins
    prm.add_initializeRequest(function (sender, args) {
        functionToBeInvoked(sender._postBackSettings.sourceElement);
        try { tools.updatePanel._logErrors(args); } catch (e) { }
    });
};
///#source 1 1 /js/tools/v2/ucBinder.js
/*
{ 
    serviceURL (scriptServiceUrl) 
    controlLocation (userControlPath || uc) 
    selector, 
    successCallback, (onSuccess || success)
    errorCallback, (errorCallback || error)
    extraQueryString (params)
    append (shouldAppend)
}
*/
var ucBinder = function (options) {
    var serviceUrl = options.serviceURL || options.scriptServiceUrl || scriptServiceUrl;
    if (!serviceUrl)
        throw "serviceURL olmadan ucBinder kullanılamaz";
    var userControlPath = options.controlLocation || options.userControlPath || options.uc;
    var extraQueryString = options.extraQueryString || options.params || "";
    var onSuccess = options.successCallback || options.onSuccess || options.success || function () { };
    var onError = options.errorCallback || options.onError || options.error || function () { };
    var shouldAppend = options.shouldAppend || options.append;

    var url = serviceUrl + "?isMobileRedirectionDisabled=1&controlLocation=" + userControlPath + "&lang=" + language + extraQueryString;
    $.ajax({
        type: "GET",
        url: url,
        timeout: options.timeout,
        dataType: 'text',
        success: function (html) {
            var $elem = $(options.selector);
            if (html == "" || html.indexOf("!!error!!") != -1) {
                $elem.remove();
                onError();
                return;
            }
            if (shouldAppend)
                $elem.show().append(html);
            else
                $elem.show().html(html);
            if (onSuccess)
                onSuccess(html, $elem);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $(options.selector).remove();
            onError(XMLHttpRequest, textStatus, errorThrown);
        }
    });
};
