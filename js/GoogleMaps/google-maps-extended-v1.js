﻿function googleMapExtended(options) {
    /// <summary>
    /// new instance of GoogleMapsExtended
    /// </summary>
    /// <param name="options" type="options">defaults: {locations:[], mapElementID: null, googleMapsOptions: null, autoFocusMarkerIndex: null}</param>
    googleMapExtended.prototype.base  = this;
    googleMapExtended.prototype.locations = options.locations;

    //Options
    googleMapExtended.prototype._defaultOptions = {
        locations: [],
        mapElementID: null, //GoogleMaps divinin id'si
        googleMapsOptions: null, //GoogleMaps'e geçilecek option'lar
        autoFocusMarkerIndex: null //
    };

    googleMapExtended.prototype.options = $.extend(googleMapExtended.prototype._defaultOptions, options);
    googleMapExtended.prototype._markersArray = [];
    googleMapExtended.prototype.map = null;
    googleMapExtended.prototype.directionsDisplay = null;
    googleMapExtended.prototype.directionsService = null;
    googleMapExtended.prototype.init();
};


//-------------------------
//POSITIONING functions
googleMapExtended.prototype.positioning = {};
googleMapExtended.prototype.positioning.fitBoundsOfMap = function (latMin, latMax, lngMin, lngMax, offset) {
    /// <summary>
    /// Haritayı belirtilen dört köşeyi gösterecek şekilde fit ettirir.
    /// </summary>
    if (typeof (offset) == "undefined")
        offset = 0.005;
    var minLatLng = new google.maps.LatLng(latMin - offset, lngMin - offset);
    var maxLatLng = new google.maps.LatLng(latMax + offset, lngMax + offset);
    var latLangBounds = new google.maps.LatLngBounds(minLatLng, maxLatLng);
    this.map.fitBounds(latLangBounds);
}
googleMapExtended.prototype.positioning.findEndPointsLatLngOfLocations = function (locations) {
    /// <summary>
    /// Lokasyon array'inin endpointlerini (uç köşelerini) ve merkezini bulu
    /// </summary>
    /// <param name="locations" type="location[]"></param>
    /// <returns type="">{center, latMin, latMax, lngMin, lngMax} object</returns>
    var lat_min = googleMapExtended.helpers.latlng.getMinLat(locations);
    var lat_max = googleMapExtended.helpers.latlng.getMaxLat(locations);
    var lng_min = googleMapExtended.helpers.latlng.getMinLng(locations);
    var lng_max = googleMapExtended.helpers.latlng.getMaxLng(locations);
    var lat_center = (lat_max + lat_min) / 2.0;
    var lng_center = (lng_max + lng_min) / 2.0;

    var centerLatLng = new google.maps.LatLng(lat_center, lng_center);
    return {
        center: centerLatLng,
        latMin: lat_min,
        latMax: lat_max,
        lngMin: lng_min,
        lngMax: lng_max
    };
}
googleMapExtended.prototype.positioning.setMapCenterRelativeToInitialLocation = function (targetLocation, fitBounds, offset) {
    throw "Bu metoda gerek yok. Kullanan birisi varsa bu referansı kaldırmalı. positioning.findEndPointsLatLngOfLocations metodu ve positioning.fitBoundsOfMap ile aynı işi yapabilir";
    /// <param name="targetLocation" type="type"></param>
    /// <param name="fitBounds" type="type"></param>
    /// <param name="offset" type="type"></param>
    //var targetLatLng = new google.maps.LatLng(targetLocation.lat, targetLocation.lng);
    //var lat_min = Math.min(targetLatLng.lat(), this.initialLocation.lat);
    //var lat_max = Math.max(targetLatLng.lat(), this.initialLocation.lat);
    //var lng_min = Math.min(targetLatLng.lng(), this.initialLocation.lng);
    //var lng_max = Math.max(targetLatLng.lng(), this.initialLocation.lng);
    //var lat_center = (lat_max + lat_min) / 2.0;
    //var lng_center = (lng_max + lng_min) / 2.0;

    //var centerLatLng = new google.maps.LatLng(lat_center, lng_center);
    //this.map.setCenter(centerLatLng);

    //if (typeof (fitBounds) != 'undefined' && fitBounds) {
    //    var offset = 0.005;
    //    var minLatLng = new google.maps.LatLng(lat_min - offset, lng_min - offset);
    //    var maxLatLng = new google.maps.LatLng(lat_max + offset, lng_max + offset);
    //    var latLangBounds = new google.maps.LatLngBounds(minLatLng, maxLatLng);
    //    this.map.fitBounds(latLangBounds);
    //}

    ///// <summary>
    ///// Otel merkezi gibi, iki konumu da ekranda tutacak şekilde, haritayı ortalar.
    ///// </summary>
    ///// <param name="callback" type="type"></param>
    //var result = gmapsHelpers.findEndPointsLatLngOfLocations(locations);
    //var centerLatLng = result.center;
    //map.setCenter(centerLatLng);

    //if (typeof (fitBounds) !== 'undefined' && fitBounds)
    //    gmapsHelpers.fitBoundsOfMap(map, result.latMin, result.latMax, result.lngMin, result.lngMax, offset);

}
googleMapExtended.prototype.positioning.centerToLocation = function (index, smoothZoom) {
    /// <summary>
    /// verilan index'deki lokasyona haritayı merkezler. smoothzoom'da yapabilir verilen parametreye göre.
    /// </summary>
    var latLng = googleMapExtended.helpers.latlng.convertToGoogleLatLng(googleMapExtended.prototype.options.locations[index]);
    googleMapExtended.prototype.map.setCenter(latLng);
    if (smoothZoom) {
        var targetZoom = googleMapExtended.prototype.options.locations[index].zoom;
        googleMapExtended.helpers.zoom.smoothZoom(googleMapExtended.prototype.map, targetZoom);
    }
};
//-------------------------



//-------------------------
//HELPERS
googleMapExtended.helpers = {
    serializeMapStylesForStaticMap: function (map) {
        /// <summary>
        /// Static map için (google imaj haritalar) bu haritanın bütün style'larını toplayıp bir url haline getirir.
        /// </summary>
        /// <returns type=""></returns>
        var styles = map.styles;
        var result = "";
        //https://maps.googleapis.com/maps/api/staticmap?center=40.993474,28.69648200000006&zoom=12&size=1200x500&maptype=roadmap
        //&markers=color:green%7Clabel:G%7C40.711614,-74.012318
        //&style=feature:water%7Ccolor:0xc0c0c0%7Cvisibility:on
        for (var i = 0; i < styles.length; i++) {
            var kvp1 = styles[i];
            if (result != "")
                result += "&";
            if (kvp1["featureType"])
                result += "style=feature:" + kvp1["featureType"];
            if (kvp1["elementType"])
                result += "|element:" + kvp1["elementType"];
            if (kvp1["stylers"]) {
                for (var j = 0; j < kvp1["stylers"].length; j++) {
                    var kvp2 = kvp1["stylers"][j];
                    for (var key in kvp2)
                        result += "|" + key + ':' + kvp2[key];
                }
            }
        }
        return result.replaceAll('#', '0x').replaceAll('|', '%7C');
    },
    findLocation: function (geocoder, searchOptions, foundCallback, notFoundCallback) {
        /// <summary>
        /// LatLng kullanarak açık adres bilgisi araması yapar. 
        /// Arama yapmak için searchOptions; { 'latLng': latlng } şeklinde latLng veya, { 'address': address, 'region': language } ile arama yapılabilir. 
        /// foundCallback results array döner, array'deki elementlerin "formatted_address" attribute'unde açık adres bilgisi bulunuyor.
        /// </summary>
        geocoder.geocode(searchOptions, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK)
                foundCallback(results);
            else
                notFoundCallback();
        });
    },
    latlng: {
        getLat: function (location) { return typeof (location["lat"]) == "function" ? location.lat() : location.lat; },
        getLng: function (location) { return typeof (location["lng"]) == "function" ? location.lng() : location.lng; },
        convertToGoogleLatLng: function (location) {
            /// <summary>
            /// Verilen location parametresi google.maps.LatLng tipinde değilse, {lat:000, lng:000} şeklinde bir obje ise, onu google'ın LatLng tipine dönüştürür.
            /// </summary>
            if (typeof location.lat == "function" && typeof location.lng == "function")
                return location;
            var result = new google.maps.LatLng(googleMapExtended.helpers.latlng.getLat(location), googleMapExtended.helpers.latlng.getLng(location));
            return result;
        },
        getMaxLat : function (locations) {
            var max = googleMapExtended.helpers.latlng.getLat(locations[0]);
            for (var i = 1; i < locations.length; i++) {
                if (max < googleMapExtended.helpers.latlng.getLat(locations[i])) {
                    max = googleMapExtended.helpers.latlng.getLat(locations[i]);
                }
            };
            return max;
        },
        getMaxLng : function (locations) {
            var max = googleMapExtended.helpers.latlng.getLng(locations[0]);
            for (var i = 1; i < locations.length; i++) {
                if (max < googleMapExtended.helpers.latlng.getLng(locations[i])) {
                    max = googleMapExtended.helpers.latlng.getLng(locations[i]);
                }
            }
            return max;
        },
        getMinLat : function (locations) {
            var min = googleMapExtended.helpers.latlng.getLat(locations[0]);
            for (var i = 1; i < locations.length; i++)
                if (min > googleMapExtended.helpers.latlng.getLat(locations[i]))
                    min = googleMapExtended.helpers.latlng.getLat(locations[i]);
            return min;
        },
        getMinLng : function (locations) {
            var min = googleMapExtended.helpers.latlng.getLng(locations[0]);
            for (var i = 1; i < locations.length; i++)
                if (min > googleMapExtended.helpers.latlng.getLng(locations[i]))
                    min = googleMapExtended.helpers.latlng.getLng(locations[i]);
            return min;
        }
    },
    zoom :{
        zoomIn : function (map) {
            map.setZoom(map.getZoom() + 1);
        },
        zoomOut : function (map) {
            map.setZoom(map.getZoom() - 1);
        },
        smoothZoom : function (map, targetZoom) {
            function _smoothZoom(map, level, count, isZoomIn) {
                // If mode is zoom in
                if (count <= 0)
                    return;
                if (isZoomIn == true) {
                    var zIn = google.maps.event.addListener(map, 'zoom_changed', function (event) {
                        google.maps.event.removeListener(zIn);
                        _smoothZoom(map, level, count - 1, isZoomIn);
                    });
                    setTimeout(function () { map.setZoom(map.getZoom() + 1); }, 80);
                } else {
                    var zOut = google.maps.event.addListener(map, 'zoom_changed', function (event) {
                        google.maps.event.removeListener(zOut);
                        _smoothZoom(map, level, count - 1, isZoomIn);
                    });
                    setTimeout(function () { map.setZoom(map.getZoom() - 1); }, 80);
                }
            }
            var zoom = map.getZoom();
            var count = Math.abs(targetZoom - zoom);
            var isZoomIn = targetZoom > zoom;
            _smoothZoom(map, targetZoom, count, isZoomIn);
        }
    }
};

//-------------------------

//-------------------------
//RoadMap : haritada yol çizimi request'i atmak/temizlemek için
googleMapExtended.prototype.roadMapDrawing = {}
googleMapExtended.prototype.roadMapDrawing.clear = function () {
    /// <summary>
    /// çizilmiş yol tariflerini temizler
    /// </summary>
    this.directionsDisplay.setMap(null);
}
googleMapExtended.prototype.roadMapDrawing.request = function (travelMode, _source, _dest, index, successCallback, failCallback) {
    /// <summary>
    /// Belirtilen parametreler ile haritada yol çizimi yapar. successCallback parametre olarak google response, distance ve duration parametreleri alır.
    /// Daha önceki sürümlerde marker da ekliyordu bu metod. artık EKLEMİYOR. callback'lerde client'ın karar vermesi lazım ne yapılacağına.
    /// </summary>
    var origin = googleMapExtended.helpers.latlng.convertToGoogleLatLng(_source);
    var destination = googleMapExtended.helpers.latlng.convertToGoogleLatLng(_dest);

    var request = { origin: origin, destination: destination, travelMode: travelMode };
    this.directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            googleMapExtended.prototype.helpers.clearDirections();
            googleMapExtended.prototype.setMapCenterRelativeToInitialLocation(_dest, true);
            googleMapExtended.prototype.directionsDisplay.setDirections(response);
            googleMapExtended.prototype.directionsDisplay.setMap(googleMapExtended.prototype.map);
            if (typeof (successCallback) !== "undefined") {
                var distance = leg.distance.text;
                var duration = leg.duration.text;
                successCallback(response, distance, duration);
            }
            //bu drawing ile ne yapmak istediğine client karar versin...

            //var firstRoute = response.routes[0];
            //var leg = firstRoute.legs[0];
            ////updateOpenPanelDurationDistance(distance, duration, location.isWalking.toLowerCase(), $accordion);
            //var endMarker = null;
            //if (location != null) {
            //    endMarker = this.addMarker(this.locationJson[index], location.content, null, this.locationJson[index].icon, location.name, location.Distance, location.icon, location.OffSetW, location.OffSetH);
            //} else {
            //    endMarker = this.addMarker(this.locationJson[index], null, null, mapIcons.end, null, null, null, _offSetW, _offSetH);
            //}

            //endMarker.setAnimation(google.maps.Animation.BOUNCE);
        } else if (typeof (failCallback) !== "undefined")
            failCallback(response);
    });
};
//-------------------------







//-------------------------
//MARKERS
googleMapExtended.prototype.markers = {};
googleMapExtended.prototype.markers._createMarkerInfobox = function (marker, location, infoboxOptions) {
    infoboxOptions = infoboxOptions || {};
    var useInfoBox = infoboxOptions.enabled || false;
    if (!useInfoBox)
        return null;

    var infoboxTemplate = infoboxOptions.template || "";
    var infoboxOffsets = infoboxOptions.offsets || { x: 0, y: 0 };
    var infoboxStyle = infoboxOptions.style || {};
    var boxText = infoboxTemplate.formatUsingObject(location);
    var alwaysOpen = infoboxOptions.alwaysOpen;

    var ibOptions = {
        content: decodeURI(boxText),
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(parseInt(infoboxOffsets.x), parseInt(infoboxOffsets.y)),
        zIndex: null,
        boxStyle: infoboxStyle,
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        map: this.map,
        visible: true,
        enableEventPropagation: false
    };
    google.maps.event.addListener(marker, 'mouseover', function (e) {
        marker.ib.open(googleMapExtended.prototype.map, this);
    });
    google.maps.event.addListener(marker, 'mouseout', function (e) {
        marker.ib.close();
    });
    google.maps.event.addListenerOnce(googleMapExtended.prototype.map, 'idle', function (e) {
        //ib.setMap(map);
        //marker.ib = ib;
        if (alwaysOpen)
            marker.ib.open(googleMapExtended.prototype.map, marker);
    });
    var ib = new InfoBox(ibOptions);
    return ib;
}
googleMapExtended.prototype.markers._createMarker = function (location, options) {
    var marker = new google.maps.Marker({
        map: googleMapExtended.prototype.map,
        draggable: false,
        position: location,
        visible: true,
        //title: name, a ile çevirip title basıyor, tooltipin üzerinde de tooltip çıkıyor :)
        clickable: true, //must be true, infobox not working otherwise!!!
        //animation: google.maps.Animation.DROP,
        icon: options.icon
    });
    if (options.bounceMarkerOnHover) {
        marker.addListener('mouseover', function () {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        });
        marker.addListener('mouseout', function () {
            marker.setAnimation(null);
        });
    }
    if (options.onMarkerClick) {
        google.maps.event.addListener(marker, 'click', function () {
            options.onMarkerClick(location);
        });
    } else if (options.url) {
        google.maps.event.addListener(marker, 'click', function () {
            window.location.href = options.url;
        });
    }

    marker.ib = googleMapExtended.prototype.markers._createMarkerInfobox(marker, location, options.infoboxOptions);
    return marker;
}
googleMapExtended.prototype.markers.addMarker = function (location, options) {
    /// <summary>
    /// Marker oluşturur. Test edilmeli, parametreleri gözden geçirilmeli. Infobox kısmı ve parametreleri ayrı metoda koyulmalı iki işi birden görmemeli bu metod!
    /// </summary>
    /// <param name="options" type="options">defaults: {icon:"", url:"", onMarkerClick: function(location), infoboxOptions : { enabled : false, template : "", style: {}, } }</param>
    /// <returns type="">marker</returns>
    var marker = googleMapExtended.prototype.markers._createMarker(location, options);
    googleMapExtended.prototype._markersArray.push(marker);
    return marker;
}
googleMapExtended.prototype.markers.clear = function () {
    for (var i = 0; i < googleMapExtended.prototype._markersArray.length; i++)
        googleMapExtended.prototype._markersArray[i].setMap(null);
    googleMapExtended.prototype._markersArray = [];
}
googleMapExtended.prototype.markers.toggleBounce = function (marker) {
    /// <summary>
    /// marker'ın zıplamasını sağlar/iptal eder.
    /// </summary>
    /// <param name="marker" type="marker"></param>
    if (marker.getAnimation() != null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}
//-------------------------



//-------------------------
//Auto Focus To Marker On Resize: Window.resize'da haritayı bir marker'a ortalatmak için
googleMapExtended.prototype.autoFocusToMarkerOnResize = {
    _eventHandler: function () {
        var marker = this._markersArray[this.options.autoFocusMarkerIndex];
        var center = marker.getPosition();
        google.maps.event.trigger(this.map, "resize");
        this.map.panTo(center);
    },
    enable: function () {
        /// <summary>
        /// Window.resize'da options.autoFocusMarkerIndex olarak belirtilen markerı ortada tutacak şekilde, haritayı ortalar.
        /// </summary>
        $(window).on("resize", this.autoFocusToMarkerOnResize._eventHandler);
    },
    disable: function () {
        /// <summary>
        /// autoFocusOnMarkerResize'ı iptal eder.
        /// </summary>
        $(window).off("resize", this.autoFocusToMarkerOnResize._eventHandler);
    }
};
//-------------------------

//-------------------------
//INIT functions
googleMapExtended.prototype._initGoogleMaps = function () {
    var mapElement = document.getElementById(this.options.mapElementID);
    if (this.options.googleMapsOptions == null) {
        this.options.googleMapsOptions = {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    }
    this.map = new google.maps.Map(mapElement, this.options.googleMapsOptions);
};

googleMapExtended.prototype._createMarkers = function () {
    this._markersArray = [];
    for (var index = 0; index < this.options.locations.length; index++) {
        var location = this.options.locations[index];
        var options = {
            url: location.url,
            icon: location.icon,
            infoboxOptions: $.extend(location.infoboxOptions || {}, this.options.infoboxOptions), //infobox options marker özelinde değişebilmesi için!
            onMarkerClick: this.options.onMarkerClick,
            bounceMarkerOnHover: this.options.bounceMarkerOnHover
        };
        var marker = this.markers.addMarker(location, options);
        this._markersArray.push(marker);
    }
}

googleMapExtended.prototype._initDirectionsRenderer = function () {
    var polylineOptions = new google.maps.Polyline({ strokeColor: '#bf1e2e', strokeOpacity: 0.7, strokeWeight: 5 });
    var rendererOptions = {
        draggable: false,
        suppressMarkers: true,
        suppressInfoWindows: true,
        hideRouteList: true,
        preserveViewport: true,
        polylineOptions: polylineOptions
    };
    this.directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
};

googleMapExtended.prototype.init = function () {
    googleMapExtended.prototype.base = this;

    this.directionsService = new google.maps.DirectionsService();
    this._initDirectionsRenderer();
    this._initGoogleMaps();

    if (this.options.autoCenter == true)
        this._autoCenterMap();

    if (this.options.autoFocusMarkerIndex)
        this.enableAutoFocusToMarkerOnResize();

    this._createMarkers();
}
//-------------------------

